#!/usr/bin/env bash
# vim:et sw=2 sts=2

set -e
set -x

function setup_remotes {
  git remote add gitlab "git@gitlab.com:hep-mirrors/$1.git"
  git remote add github "git@github.com:hep-mirrors/$1.git"
}

function updatesvn {
  REPO_NAME="$1"
  SVN_URL="$2"
  if [ ! -d "$REPO_NAME" ]; then
    mkdir "$REPO_NAME"
    pushd "$REPO_NAME" > /dev/null
    git svn init "$SVN_URL" --stdlayout --prefix=svn/
    setup_remotes "$REPO_NAME"
    popd > /dev/null
  fi
  pushd "$REPO_NAME" > /dev/null
  git svn fetch
  git svn rebase
  for branch in `git branch -r | grep "branches/" | sed 's/ branches\///'`; do
    git branch "$branch" "refs/remotes/$branch" || true
  done
  for tag in `git branch -r | grep "tags/" | sed 's/ tags\///'`; do
    git tag -a -m "import from SVN" "$tag" "refs/remotes/$tag" || true
  done
  git push --prune --all -f gitlab
  git push --prune --tags -f gitlab
  git push --prune --all -f github
  git push --prune --tags -f github
  popd > /dev/null
}

function updatehg {
  REPO_NAME="$1"
  HG_URL="$2"
  LOCAL_GIT_REPO="$REPO_NAME-git"
  if [ -d "$REPO_NAME" ]; then
    rm -rf "$REPO_NAME"
  fi
  if [ -d "$LOCAL_GIT_REPO" ]; then
    rm -rf "$LOCAL_GIT_REPO"
  fi
  mkdir "$LOCAL_GIT_REPO"
  pushd "$LOCAL_GIT_REPO" > /dev/null
  git init
  setup_remotes "$REPO_NAME"
  popd > /dev/null
  hg clone --insecure "$HG_URL" "$REPO_NAME"
  pushd "$REPO_NAME" > /dev/null
  hg bookmarks hg
  HG_BRANCHES=`hg branches -q`
  for branch in $HG_BRANCHES; do
    hg bookmark -r $branch bm-$branch
  done
  hg push "../$LOCAL_GIT_REPO"
  popd > /dev/null
  pushd "$LOCAL_GIT_REPO" > /dev/null
  git push -f gitlab hg:master
  git push -f github hg:master
  for branch in $HG_BRANCHES; do
    git push -f gitlab bm-$branch:$branch
    git push -f github bm-$branch:$branch
  done
  popd > /dev/null
}

function updategit {
  REPO_NAME="$1"
  GIT_URL="$2"
  # http://blog.plataformatec.com.br/2013/05/how-to-properly-mirror-a-git-repository/
  # https://stackoverflow.com/questions/34265266/remote-rejected-errors-after-mirroring-a-git-repository
  if [ ! -d "$REPO_NAME" ]; then
    mkdir "$REPO_NAME"
    pushd "$REPO_NAME" > /dev/null
    git init --bare
    git remote add origin "$GIT_URL"
    git config --unset-all remote.origin.fetch
    git config --add remote.origin.fetch '+refs/heads/*:refs/heads/*'
    git config --add remote.origin.fetch '+refs/tags/*:refs/tags/*'
    setup_remotes "$REPO_NAME"
    popd > /dev/null
  fi
  pushd "$REPO_NAME" > /dev/null
  git remote update --prune origin
  git push --prune --all -f gitlab
  git push --prune --tags -f gitlab
  git push --prune --all -f github
  git push --prune --tags -f github
  popd > /dev/null
}

"$1" "$2" "$3"
