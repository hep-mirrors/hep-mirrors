{
  outputs = { self, nixpkgs }:
    let

      inherit (nixpkgs) lib;
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" ];

    in
    {

      devShells = lib.genAttrs supportedSystems
        (system:
          with import nixpkgs {
            inherit system;
          };
          {
            default =
              mkShell {
                buildInputs = [
                  bash
                  gitFull
                  mercurial
                  python3Packages.hg-git
                  openssh
                ];
              };
          });

    };
}
